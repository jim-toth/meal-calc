import { expect } from 'chai';
import sinon from 'sinon';
import axios, { AxiosResponse } from 'axios';

import ApiService, { apiBase } from '../../../src/services/ApiService';

const validApiKey = 'VALID_API_KEY';
const invalidApiKey = 'INVALID_API_KEY';

sinon.stub(axios, 'get').callsFake(
  (url: string): Promise<AxiosResponse<any>> => {
    return new Promise<AxiosResponse<any>>(resolve => {
      const params = `api_key=${validApiKey}&pageSize=1`;
      if (url === `${apiBase}/foods/list?${params}`) {
        resolve({
          data: {},
          status: 200,
          statusText: 'OK',
          headers: {},
          config: {},
          request: {}
        });
      } else {
        resolve({
          data: {},
          status: 403,
          statusText: 'Forbidden',
          headers: {},
          config: {},
          request: {}
        });
      }
    });
  }
);

let apiService: ApiService;

describe('ApiService', () => {
  beforeEach(() => {
    apiService = new ApiService();
  });

  it('checks valid API keys', async () => {
    const valid = await apiService.checkApiKey(validApiKey);

    expect(valid).to.equal(true);
  });

  it('checks invalid API keys', async () => {
    const invalid = await apiService.checkApiKey(invalidApiKey);

    expect(invalid).to.equal(false);
  });
});
