import { expect } from 'chai';
import sinon from 'sinon';
import { shallowMount, Wrapper } from '@vue/test-utils';

import ApiKeyForm from '../../../src/components/ApiKeyForm.vue';

const validApiKey = 'VALID_API_KEY';

const apiServiceMock = {
  checkApiKey: sinon.stub().callsFake((apiKey: string) => {
    return apiKey === validApiKey;
  })
};

const $storeMock = {
  state: { apiKey: '' },
  commit: sinon.stub()
};

let wrapper: Wrapper<ApiKeyForm>;

describe.only('ApiKeyForm', () => {
  beforeEach(() => {
    apiServiceMock.checkApiKey.resetHistory();
    $storeMock.commit.resetHistory();
  });

  describe('when no API key is stored', () => {
    beforeEach(() => {
      $storeMock.state.apiKey = '';

      wrapper = shallowMount(ApiKeyForm, {
        data: () => {
          return {
            apiService: apiServiceMock
          };
        },
        mocks: {
          $store: $storeMock
        }
      });
    });

    it('should not display status if not yet checked', () => {
      expect(wrapper.contains('span.ApiKeyForm-status')).to.equal(false);
    });

    it('should set valid API keys in the store and display ✔️', async () => {
      wrapper.setData({ apiKey: validApiKey });
      wrapper.find('button').trigger('click');
      await wrapper.vm.$nextTick();
      const status = wrapper.find('span.ApiKeyForm-status');

      expect(apiServiceMock.checkApiKey.calledOnce).to.equal(true);
      expect(apiServiceMock.checkApiKey.calledWith(validApiKey)).to.equal(true);
      expect($storeMock.commit.calledOnce).to.equal(true);
      expect($storeMock.commit.calledWith('apiKey', validApiKey)).to.equal(
        true
      );
      expect(status.text()).to.equal('✔️');
    });

    it('should not set invalid API keys and display ❌', async () => {
      const invalidKey = 'invalid key';

      wrapper.setData({ apiKey: invalidKey });
      wrapper.find('button').trigger('click');
      await wrapper.vm.$nextTick();
      const status = wrapper.find('span.ApiKeyForm-status');

      expect(apiServiceMock.checkApiKey.calledOnce).to.equal(true);
      expect(apiServiceMock.checkApiKey.calledWith(invalidKey)).to.equal(true);
      expect($storeMock.commit.called).to.equal(false);
      expect(status.text()).to.equal('❌');
    });
  });

  describe('when an API key is stored', () => {
    beforeEach(() => {
      $storeMock.state.apiKey = validApiKey;

      wrapper = shallowMount(ApiKeyForm, {
        data: () => {
          return {
            apiService: apiServiceMock
          };
        },
        mocks: {
          $store: $storeMock
        }
      });
    });

    it('should show stored API key is valid', () => {
      const input = wrapper.find('input.ApiKeyForm-input');
      const status = wrapper.find('span.ApiKeyForm-status');

      expect((input.element as HTMLInputElement).value).to.equal(validApiKey);
      expect(status.text()).to.equal('✔️');
    });
  });
});
