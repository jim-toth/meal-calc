import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    apiKey: ''
  },
  mutations: {
    apiKey(state, newApiKey) {
      state.apiKey = newApiKey
    }
  },
  actions: {},
  modules: {}
})
