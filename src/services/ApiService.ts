import axios from 'axios';

const apiBase = 'https://api.nal.usda.gov/fdc/v1';

export default class ApiService {
  async checkApiKey(apiKey: string): Promise<boolean> {
    const params = `api_key=${apiKey}&pageSize=1`;

    try {
      const response = await axios.get(`${apiBase}/foods/list?${params}`);

      return response.status === 200;
    } catch {
      return false;
    }
  }
}

export { apiBase };
